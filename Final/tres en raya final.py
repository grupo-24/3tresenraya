import pygame
import numpy as np
import time

def espera_click(matriz, dimAncho, dimAlto, simbolo):
    comprobador = True
    while comprobador:
        ev = pygame.event.get()
        for event in ev:
            if event.type == pygame.QUIT:
                exit(1)
            mouse_click = pygame.mouse.get_pressed()

            if mouse_click[0] > 0:
                posX, posY = pygame.mouse.get_pos()
                celX, celY = int(np.floor(posX / dimAncho)), int(np.floor(posY / dimAlto))
                if matriz[celX][celY] == " ":
                    matriz[celX][celY] = simbolo
                    print("matriz")
                    mostrar(matriz)
                    print("")
                    comprobador = False
                    break
    return matriz

def refrescar_pantalla(pantalla, matriz, n_celdas_x, n_celdas_y,dimAncho, dimAlto):
    for y in range(3):
        for x in range(3):
            poligono = [
                            ((x)    * dimAncho, y     * dimAlto),
                            ((x+1)  * dimAncho, y     * dimAlto),
                            ((x+1)  * dimAncho, (y+1) * dimAlto),
                            ((x)    * dimAncho, (y+1) * dimAlto)
                        ]
            poligono_p1 = [
                            ((x)    * dimAncho - 1, y     * dimAlto - 1),
                            ((x+1)  * dimAncho - 1, y     * dimAlto - 1),
                            ((x+1)  * dimAncho - 1, (y+1) * dimAlto - 1),
                            ((x)    * dimAncho - 1, (y+1) * dimAlto - 1)
                          ]

            
            if matriz[x][y] == " ":
                pygame.draw.polygon(pantalla, (0, 0, 0), poligono, 2)
            elif matriz[x][y] == "X":
                pygame.draw.polygon(pantalla, (255, 255, 255), poligono_p1, 0)
            else:
                pygame.draw.polygon(pantalla, (255, 0, 0), poligono_p1, 0)
    pygame.display.flip()


def mostrar(matriz):
    for i in range(3):
        for j in range(3):
            print("|",matriz[i][j],"|",end="")
        print("")


def tabLLeno(matriz):
    x=0
    for i in range(3):
        for j in range(3):
            if matriz[i][j] == "X" or matriz[i][j] == "O":
                x+=1

    if x==9:
        return True
    return False


def comprobarGanador(matriz,jugador):

    if matriz[0][0]==jugador and matriz[0][1]==jugador and matriz[0][2]==jugador :
        return True
    
    elif matriz[1][0]==jugador and matriz[1][1]==jugador and matriz[1][2]==jugador :
        return True
    
    elif matriz[2][0]==jugador and matriz[2][1]==jugador and matriz[2][2]==jugador :
        return True
    
    elif matriz[0][0]==jugador and matriz[1][0]==jugador and matriz[2][0]==jugador :
        return True

    elif matriz[0][1]==jugador and matriz[1][1]==jugador and matriz[2][1]==jugador :
        return True
    
    elif matriz[0][2]==jugador and matriz[1][2]==jugador and matriz[2][2]==jugador :
        return True

    elif matriz[0][0]==jugador and matriz[1][1]==jugador and matriz[2][2]==jugador :
        return True
    
    elif matriz[0][2]==jugador and matriz[1][1]==jugador and matriz[2][0]==jugador :
        return True

    else:
        return False


def sumfcd(matriz,jugador):
    sumac=0
    sumac2=0
    sumjc=0

    for i in range(3):
        ac=0
        jc=0
        for j in range(3):
            if matriz[i][j] == jugador or matriz[i][j] == " ":
               ac+=1
               sumac2=ac
               if  matriz[i][j] == jugador:
                   jc+=1
                   sumjc=jc
        
        if sumjc >= 1 and sumac2 == 3:
            sumac+=1
        
        sumjc=0
        sumac2=0


    for i in range(3):
        ac=0
        jc=0
        for j in range(3):
            if matriz[j][i] == jugador or matriz[j][i] == " ":
               ac+=1
               sumac2=ac
               if  matriz[j][i] == jugador:
                   jc+=1
                   sumjc=jc
        
        if sumjc >= 1 and sumac2 == 3:
            sumac+=1
        
        sumjc=0
        sumac2=0
    

    ac=0
    jc=0
    for i in range(3):
        if matriz[i][i] == jugador or matriz[i][i] == " ":
            ac+=1
            sumac2=ac
            if  matriz[i][i] == jugador:
                jc+=1
                sumjc=jc
        
        if sumjc >= 1 and sumac2 == 3:
            sumac+=1
            sumjc=0
            sumac2=0
    
    auxr=2
    ac=0
    jc=0
    for i in range(3):
        if matriz[i][auxr] == jugador or matriz[i][auxr] == " ":
            ac+=1
            sumac2=ac
            if  matriz[i][auxr] == jugador:
                jc+=1
                sumjc=jc
        auxr-=1
        
        if sumjc >= 1 and sumac2 == 3:
            sumac+=1
            sumjc=0
            sumac2=0


    # print("sum=",sumac)
    return sumac


def revisarCasilla(matriz,i,j):
    if matriz[i][j]==" ":
        return True
    else:
        return False


def copiarDatos(matriz1,matriz2):
    for i in range(3):
        for j in range(3):
            matriz1[i][j]=matriz2[i][j]

    return matriz1


def conpe(matrizx):
    tablerov12=[
        [" "," "," "],
        [" "," "," "],
        [" "," "," "]
    ]
    tablerov12=copiarDatos(tablerov12,matrizx)
    
    for i in range(3):
                for j in range(3):
                    if revisarCasilla(tablerov12,i,j):
                        tablerov12[i][j]="X"
                        if comprobarGanador(tablerov12,"X"):
                            return True
                        else:
                            tablerov12=copiarDatos(tablerov12,matrizx)
    return False


if __name__ == "__main__":

    sj=False

    tablero=[
        [" "," "," "],
        [" "," "," "],
        [" "," "," "]
    ]

    tablero2=[
        [" "," "," "],
        [" "," "," "],
        [" "," "," "]
    ]

    print ('...:TABLERO:...')
    mostrar(tablero)
    intercalarj=True
    sumMIn=0
    sumMInn=0
    c=1
    tablero3=[
        [" "," "," "],
        [" "," "," "],
        [" "," "," "]
    ]

    tablero4=[
        [" "," "," "],
        [" "," "," "],
        [" "," "," "]
    ]
    c2=1
    respuesta=1



    pygame.init()
    width, height = 500, 500 # tamaño de la pantalla
    pantalla = pygame.display.set_mode((height, width))
    color_fondo = 0, 196, 185 # turquesa
    pantalla.fill(color_fondo) # pinta la pantalla de turquesa 

    n_celdas_x, n_celdas_y = 3, 3 # numero de celdas

    dimAncho = width / n_celdas_x # calcula el tamaño de cada celda a lo ancho
    dimAlto = height / n_celdas_y # calcula el tamaño de cada celda a lo largo

    simbolo="X"

    while sj==False and tabLLeno(tablero)==False:
        
        pantalla.fill(color_fondo) 

        px=0
        po=0
        
        refrescar_pantalla(pantalla, tablero, n_celdas_x, n_celdas_y, dimAncho, dimAlto)

        if intercalarj==True:
            tablero=espera_click(tablero, dimAncho, dimAlto, simbolo)

            intercalarj=False
            tablero2=copiarDatos(tablero2,tablero)
            sj=comprobarGanador(tablero,"X")
            if sj==True:
                refrescar_pantalla(pantalla, tablero, n_celdas_x, n_celdas_y, dimAncho, dimAlto)

                time.sleep(2)

                pantalla.fill(color_fondo)
                pygame.display.flip()
                fuente=pygame.font.Font(None,35)
                texto=fuente.render("GANA O !!!!!!!!!!",0,(0,0,0))
                pantalla.blit(texto,(200,250))
                                
                pygame.display.update()

                time.sleep(3)
                print("GANA X !!!!!!!!!!")
                exit(1)


        else:
            
            for i in range(3):
                for j in range(3):
                    # print("tablero 1: ",tablero)
                    # print("tablero 2: ",tablero2)
                    # print("tablero 3: ",tablero3)
                    if revisarCasilla(tablero,i,j)==True:
                        if c == 1:
                            tablero2[i][j]="O"
                            px=sumfcd(tablero2,"X")
                            po=sumfcd(tablero2,"O")
                            sumMIn=px-po
                            tablero3=copiarDatos(tablero3,tablero2)
                            tablero2=copiarDatos(tablero2,tablero)
                            c=999
                        else:
                            tablero2[i][j]="O"
                            px=sumfcd(tablero2,"X")
                            po=sumfcd(tablero2,"O")
                            sumMInn=px-po
                            if comprobarGanador(tablero2,"O"):
                                i=4
                                j=4
                                tablero=copiarDatos(tablero,tablero2)

                                refrescar_pantalla(pantalla, tablero, n_celdas_x, n_celdas_y, dimAncho, dimAlto)

                                time.sleep(2)

                                pantalla.fill(color_fondo)
                                pygame.display.flip()
                                fuente=pygame.font.Font(None,35)
                                texto=fuente.render("GANA O !!!!!!!!!!",0,(0,0,0))
                                pantalla.blit(texto,(200,250))
                                
                                pygame.display.update()

                                time.sleep(3)
                                print("GANA O !!!!!!!!!!")
                                exit(1)

                            else:
                                if sumMInn < sumMIn and conpe(tablero2)==False:
                                    sumMIn=sumMInn
                                    tablero3=copiarDatos(tablero3,tablero2)
                                    tablero2=copiarDatos(tablero2,tablero)
                                elif respuesta>=2 and conpe(tablero2)==False :
                                    sumMIn=-99
                                    tablero3=copiarDatos(tablero3,tablero2)
                                    tablero2=copiarDatos(tablero2,tablero)
                                else:
                                    tablero2=copiarDatos(tablero2,tablero)
                    # print(tablero3)
            
            c=1 
            respuesta+=1             
            intercalarj=True
            tablero=copiarDatos(tablero,tablero3)
            
            sj=comprobarGanador(tablero,"O")

            
        
        print("")
        print("")
        mostrar(tablero)
        refrescar_pantalla(pantalla, tablero, n_celdas_x, n_celdas_y, dimAncho, dimAlto)

    if tabLLeno(tablero):
        time.sleep(2)

        pantalla.fill(color_fondo)
        pygame.display.flip()
        fuente=pygame.font.Font(None,35)
        texto=fuente.render("EMPATE !!!!!!!!!",0,(0,0,0))
        pantalla.blit(texto,(200,250))
                                
        pygame.display.update()

        time.sleep(3)
        print("empate!!!!!!!!!")
        exit(1)
